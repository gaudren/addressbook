""" Address book. First name, last name, email
    Can either add, delete or search"""

contacts = []

def addContact(contacts):
    newContact = {}

    name = input("First Name: ")
    surname = input("Last Name: ")
    email = input("Email: ")

    newContact["fName"] = name
    newContact["lName"] = surname
    newContact["mail"] = email

    contacts.append(newContact)

def printContact(contacts):
    for (i, x) in enumerate(contacts):
        print(str(i) + ": ", x["fName"], x["lName" ], x["mail"])

def searchContact(contacts, needle):
    found = False
    for x in contacts:
        if x["fName"] == needle:
            found = True
            print(x["fName"], x["lName" ], x["mail"])
            print()
        elif x["lName"] == needle:
            found = True
            print(x["fName"], x["lName" ], x["mail"])
            print()
        elif x["mail"] == needle:
            found = True
            print(x["fName"], x["lName" ], x["mail"])
            print()
    if not found:
        print("Contact not found!")
        print()

def deleteContact(contacts, i):
    del contacts[i]
        

print("Simple Address Book!")
print()
while True:
    selection = int(input("1. Add \n2. Search \n3. Delete \n4. List \n5. Quit \n> "))

    if selection == 1:
        addContact(contacts)
        print()
        print("Contacts: \n")
        printContact(contacts)
        print()
    elif selection == 2:
        needle = input("Search: ")
        searchContact(contacts, needle)
    elif selection == 3:
        index = int(input("Index number to delete: "))
        deleteContact(contacts, index)
    elif selection == 4:
        print()
        printContact(contacts)
    elif selection == 5:
        break
    else:
        print("Invalid selection!")
        print()
        